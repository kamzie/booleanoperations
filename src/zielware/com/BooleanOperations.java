package zielware.com;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public abstract class BooleanOperations implements AndOperation, OrOperation {

	protected Map<String, Integer> genericMap;
	private int howManyVarArgs = 0;
	private int howManyAnds = 0;
	private int howManyOrs = 0;
	StringBuilder stringBuilder;

	protected BooleanOperations(Map<String, Integer> genericMap) {
		this.genericMap = genericMap;
	}

	@Override
	public String or(final String... s) {

		calculateStringOccurences(s);
		stringBuilder = generateStringOr();
		clearStringOccurences();

		return stringBuilder.toString();
	}

	@Override
	public String and(String... s) {

		calculateStringOccurences(s);
		stringBuilder = generateStringAnd();
		clearStringOccurences();

		return stringBuilder.toString();
	}

	private StringBuilder generateStringOr() {

		stringBuilder = new StringBuilder();

		genericMap.forEach((k, v) -> {
			if (v > 0) {
				if (howManyOrs > 0) {
					stringBuilder.append(",");
				}
				stringBuilder.append(k);
				howManyOrs++;
			}
		});
		return stringBuilder;
	}

	private StringBuilder generateStringAnd() {

		stringBuilder = new StringBuilder();

		genericMap.forEach((k, v) -> {
			if (v == howManyVarArgs) {
				if (howManyAnds > 0) {
					stringBuilder.append(",");
				}
				stringBuilder.append(k);
				howManyAnds++;
			}
		});
		return stringBuilder;
	}

	private void clearStringOccurences() {
		genericMap.forEach((k, v) -> v = 0);
	}

	private void calculateStringOccurences(final String... s) {
		for (String element : s) {
			List<String> items = Arrays.asList(element.split("\\s*,\\s*"));

			items.forEach(item -> {
				if (genericMap.containsKey(item)) {
					int incrementedValue = genericMap.get(item) + 1;
					String tempKey = item;
					genericMap.put(tempKey, incrementedValue);
				}
			});
			howManyVarArgs++;
		}
	}
}