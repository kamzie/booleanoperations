package zielware.com;

import java.util.HashMap;
import java.util.Map;

public class AllowedActions extends BooleanOperations {

	private static Map<String, Integer> allowedActions;
	static {
		allowedActions = new HashMap<String, Integer>();
		allowedActions.put("add", 0);
		allowedActions.put("delete", 0);
		allowedActions.put("modify", 0);
		allowedActions.put("clear", 0);
		allowedActions.put("hide", 0);
	}
	
	public AllowedActions () {
		super(allowedActions);
	}
}
