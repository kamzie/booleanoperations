package zielware.com;

import java.util.HashMap;
import java.util.Map;

public class SocialMediaNetworks extends BooleanOperations {

	private static Map<String, Integer> socialMediaNetworks;
	static {
		socialMediaNetworks = new HashMap<String, Integer>();
		socialMediaNetworks.put("facebook", 0);
		socialMediaNetworks.put("twitter", 0);
		socialMediaNetworks.put("youtube", 0);
		socialMediaNetworks.put("instagram", 0);
		socialMediaNetworks.put("snapchat", 0);
	}

	public SocialMediaNetworks() {
		super(socialMediaNetworks);
	}
}